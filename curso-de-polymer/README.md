# Curso de Polymer

[Curso de Platzi de Polymer y Web Components](https://platzi.com/clases/polymer/)

# Introducción

Los Web Components, nos ayudan a mejorar la web de hoy, para hacer más fácil su lectura. Con Web Components, podemos construir bloques personalizados para que funcionen, por medio de etiquetas personalizadas.
Polymer es una forma de implementar la Ingeniería de software basada en componentes en la web para crear componentes modulares y reusables. 
Los genios detras del desarrollo de un navegador web nos ofrecen con web components la posibilidad  de utilizar el mas hermoso de los frameworks hechos y que todo mundo ha ignorado, el DOM , ya que cuando alguien agrega un tag por ejemplo <audio> en realidad behind the scenes existen un conjunto de divs y otros tags, con eventos y propiedades expuestas para que el desarrollador pueda utilizarlas, asi de la misma manera podremos crear componentes siguiendo este principio.

## Utilizaremos en nuestro proyecto las siguientes herramientas: 

* Bower
* NPM
* WebComponents JS

## Qué necesitamos aprender para desarrollar Web Components?

* [Custom Elements](http://www.html5rocks.com/en/tutorials/webcomponents/customelements/):

A capability for crating your own custom HTML Tags and elements. They can have their own scripted behavior and CSS styling. They are part of Web Components but they can also be used by themselves. 

* [Shadow DOM](http://webcomponents.org/articles/introduction-to-shadow-dom/): 

Provides encapsulation for the JavaScript, CSS and templating in a Web Component. Shadow DOM makes it so these things remain separate from de DOM of the main document. You can also use Shadow DOM by itself, outside of a web component. 

* [HTML Templates](http://webcomponents.org/articles/introduction-to-template-element/):  

The HTML template element <template> is a mechanism for holding client-side content that is not to be rendered when a page is loaded but may subsequently be instantiated during runtime using JavaScript

* HTML Imports: 

HTML Imports is intended to be the packaging mechanism for Web Components, but you can also use HTML Imports by itself. 

## Bootstrapping

1. bower init
2. bower install webcomponentsjs --save
3. bower install
4. cd Demo
5. polyserve
6. Go to 127.0.0.1/8080/components/Demo/ to see if it works ok. 
7. Go to polymer-element/
8. bower install


## Links

* [Polymer](https://www.polymer-project.org/)
* [WebComponents](http://webcomponents.org/)
* [CustomElements.io](https://customelements.io/)
* [Bower](https://bower.io/)
* [NPM](https://www.npmjs.com/)
* [Semantic Versioning](https://semver.org/)


## Test Web Component 
Realizamos un test de la implementación de un Web Component sin librerías, utilizando solo un polyfill como WebComponentJS y codeando el Web Component con Vainilla. Esto lo pueden encontrar en la carpeta Demo. 