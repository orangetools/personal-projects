Building a RESTfull API with Node

We want to create an API that: 
1 - Listens on a PORT and accepts incoming HTTP requests for POST, GET, PUT, DELETE and HEAD
2 - Allows a client to connect, then create a new user, then edit and delete that user.
3 - Allows a user to "sign in" wich gives them a token that theu can use for subsequent authenticated requests.
4 - Allows the user to "sign out" wich invalidates their token. 
5 - Allows a signed-in user to use their token to create a new "check" (check a given URL to see if it's up or down) and bw able to find what up or down is (404, or anything is not 500. It depends of the particulary URL or WebSite).
6 - Allows to a signed-in user to wdit or delete any of their checks. We will limit the user checks to 5. The user can't create a 6th ckeck. 
7 - In the Background, workers perform all the "checks" at appropiate times, and send alerts to the users when a check changes its state from "up" to "down" and viseversa.
Once a minute, we want to check all the "Checks" for all the users, check the URL's with the given parameters by the user, and if its down when it have to be up, send a SMS to the user, and when is "up" but it has to be "down" we want to send the user a SMS. Otherwise, do nothing.

We will be sending SMS, so we'll do this via Twilio API, but connecting through NodeJS. 
We will be using the filesystem as a key-value store fo JSON docs, but in Production (a real app) we need to use a DB. 
