/*
 * Primary File for the API
 *
 *
 */

 //Dependencies
 var http = require('http');
 var url = require('url');
 var StringDecoder = require('string_decoder').StringDecoder;

//For starting a HTTP Server, we need to use the http module to define what the server does
//and later we need to tell server to listen on a specific port. 

//The server should respond to all requests with a string

var server = http.createServer(function(req, res){
	
	// Get the url and parse it
	var parsedUrl = url.parse(req.url, true); // The true parameter tell url lib to use the query string module 
	// After this, parsedUrl is a bunch of keys that we can use. 

	// Get the path 
	var path = parsedUrl.pathname; // This is the untrimmed path, but we watn the trimmed path. 
	var trimmedPath = path.replace(/^\/+|\/+$/g,''); // This just trim off any slash or simbol from the path.


	// Get the query string as an object
	// Now we want to get the query string object so we can use it later. 
	var queryStringObject = parsedUrl.query; // This is the same respone if we use the query string object from query string library built in NodeJS

	// Get the HTTP Method
	// This method is one of the req object, so we're calling that method. 
	var method = req.method.toLowerCase(); 

	// Get the headers as an object
	var headers = req.headers;

	// Get the payload, if there is any
	
	
	// Payloads that come in the HTTP request, comes as 'Strings', and we need to collect those strings
	// and then when the string tells us that we are at the end, code as that into one code here and the thing
	// before we can figure out what that payload is because we receive those characters una at the time, but
	// we're interested on the entire payload. 
	// So, we create a buffer and as the characters comes in we append it to the buffer untill the end
	// We do that by binding to an event that the req Object emmits and the event is called "data". 
	var decoder = new StringDecoder('utf-8'); 
	var buffer = '';
	req.on('data', function(data){
		buffer += decoder.write(data);
	});
	req.on('end', function(){
		buffer += decoder.end();


		// Choose the handler this request should go to. If one is not found, use the notFound handler
		var chosenHandler = typeof(router[trimmedPath]) !== 'undefined' ? router[trimmedPath] : handlers.notFound;

		// Construct the data object to send to the handler
		var data = {
			'trimmedPath' : trimmedPath,
			'queryStringObject' : queryStringObject,
			'method' : method,
			'headers' : headers,
			'payload' : buffer
		};

		// Router the request to the handler pecified in the router
		chosenHandler(data, function(statusCode, payload){
			// Use the status code called back by the handler, or default to 200
			statusCode = typeof(statusCode) == 'number' ? statusCode : 200;

			// Use the payload code called back by the handler, or default to empty object
			payload = typeof(payload) == 'object' ? payload : {};

			// Convert the payload to a string
			var payloadString = JSON.stringify(payload);

			// Return the response
			res.setHeader('Content-Type', 'application/json');
			res.writeHead(statusCode);
			res.end(payloadString);

			// Console Log the request path
			console.log( 'Returning this response: ', statusCode, payloadString);
		});
	});
});


// Start the server, and have it listen on port 3000
server.listen(3000, function(){
	console.log('The server is listening on port 3000 now');
});

// Define the handlers
var handlers = {};

// Foo handler
handlers.foo = function(data,callback){
	// Callback a http status code, and a payload object
	callback(200, {'message' : 'Hi! You Alien'});
};

// Sample handler
handlers.sample = function(data,callback){
	// Callback a http status code, and a payload object
	callback(406, {'name' : 'sample handler'});
};

// Not found handler
handlers.notFound = function(data,callback){
	callback(404);
};

// Define a request router

var router = {
	'sample' : handlers.sample,
	'foo' : handlers.foo
};