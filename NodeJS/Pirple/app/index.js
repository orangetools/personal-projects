/*
 * Primary File for the API
 *
 *
 */

 //Dependencies
 var http = require('http');
 var url = require('url');
 var StringDecoder = require('string_decoder').StringDecoder;

//For starting a HTTP Server, we need to use the http module to define what the server does
//and later we need to tell server to listen on a specific port. 

//The server should respond to all requests with a string

var server = http.createServer(function(req, res){
	
	// Get the url and parse it
	var parsedUrl = url.parse(req.url, true); // The true parameter tell url lib to use the query string module 
	// After this, parsedUrl is a bunch of keys that we can use. 

	// Get the path 
	var path = parsedUrl.pathname; // This is the untrimmed path, but we watn the trimmed path. 
	var trimmedPath = path.replace(/^\/+|\/+$/g,''); // This just trim off any slash or simbol from the path.


	// Get the query string as an object
	// Now we want to get the query string object so we can use it later. 
	var queryStringObject = parsedUrl.query; // This is the same respone if we use the query string object from query string library built in NodeJS

	// Get the HTTP Method
	// This method is one of the req object, so we're calling that method. 
	var method = req.method.toLowerCase(); 

	// Get the headers as an object
	var headers = req.headers;

	// Get the payload, if there is any
	
	
	// Payloads that come in the HTTP request, comes as 'Strings', and we need to collect those strings
	// and then when the string tells us that we are at the end, code as that into one code here and the thing
	// before we can figure out what that payload is because we receive those characters una at the time, but
	// we're interested on the entire payload. 
	// So, we create a buffer and as the characters comes in we append it to the buffer untill the end
	// We do that by binding to an event that the req Object emmits and the event is called "data". 
	

	



});


// Start the server, and have it listen on port 3000
server.listen(3000, function(){
	console.log('The server is listening on port 3000 now');
});


