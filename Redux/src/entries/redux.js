import { createStore } from 'redux';

const $form = document.getElementById('form');
$form.addEventListener('submit', handleSubmit);

function handleSubmit(event) {
    // Por defecto, un form recarga la pagina porque envia GET o POST los datos. Deshabilito la opcion con 'preventDefault()'.
    event.preventDefault();
    const data = new FormData($form);
    const title = data.get('title');
    console.log(title);
    // Envío nuestra acción al Store:
    store.dispatch(
        //Aqui comienza nuestra acción
        {
            type: 'ADD_SONG',
            payload: {
                title
                // artist,
                // author
            }
        }
    )
}

const initialState = [
    {
        "title": "Nada",
    },
    {
        "title": "En Tu Mar",
    },
    {
        "title": "One More Time",
    }
]

const reducer = function(state, action) {
    switch(action.type) {
        case 'ADD_SONG':
            return [...state, action.payload]
        default: 
            return state
    }
}

const store = createStore(
    // reducer,
    reducer,
    // initialState,
    initialState,
    // enhancer
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

/* const $container = document.getElementById('playlist');
const playlist = store.getState();
playlist.forEach((item) => {
    const template = document.createElement('p');
    template.textContent = item.title;
    $container.appendChild(template);
});
 */
function render() {
    const $container = document.getElementById('playlist');
    const playlist = store.getState();
    $container.innerHTML = '';
    playlist.forEach((item) => {
        const template = document.createElement('p');
        template.textContent = item.title;
        $container.appendChild(template);
    });
}
render();

function handleChange(){
    render();
}

store.subscribe(handleChange)