# Curso de Redux
## platzi.com/clases/redux

### Clase 3
_Redux:_ es una librería creada por **@dan_abramov**, para manipular los datos de una forma separada de la interfaz, similar a lo que hace webpack con la opción hot-reload.

_Redux is a predictable state container for JavaScripts Apps_

Es un contenedor del estado predecible para aplicaciones de JavaScript de front-end complejas.

_Componentes en Redux:_
- Store: Es el centro y la verdad de todo, con métodos para actualizar, obtener y escuchar datos.
- Actions: Son bloques de información que envian datos desde la aplicación hacia el store.
- Reducers: Cambian el estado de la aplicación.


#### Principios de Redux
1. _Única fuente de verdad:_
El estado de toda tu aplicación esta almacenado en un árbol guardado en un único store lo que hace mas fácil el proceso de depuración.
    >Por cada aplicación (especialmente las de tipo single page), deberías tener un solo store, excepto si tienes múltiples páginas, donde cada página por conveniencia pudiera manejar su propia store

2. _Solo Lectura:_
La única forma de modificar el estado es emitiendo una acción, un objeto que describe que ocurrió.

3. _Los cambios se realizan con funciones puras:_
Los reduces son funciones puras que toman el estado anterior y una acción, y devuelven un nuevo estado.
    >Dentro de los principios de la programación funcional, una función pura, es aquella que únicamente devuelve un resultado a partir de sus parámetros, y no hace nada más que eso, no modifica estados, no dispara eventos, etc.

Redux ❤️ React. Puedes utilizarlo con VanillaJS o con cualquier otra librería o framework (Vue, Angular o hasta con jQuery).

#### Ciclo
- Tienes tu vista (UI)
- Tu vista va a enviar una acción
- Tu acción va a llamar un reducer
- Tu reducer va a llamar a tu store
- Tu store va a actualizar el estado
- El estado va a actualizar tu vista (UI)
![Redux Cycle](./ClassesUtils/ReduxCycle.png)


### Clase 7
**_Store_** = Es la parte más importante de entender cuando estamos haciendo algo en Redux.

_Datos importantes del Store:_

- Contiene el estado de la aplicación.
- Se puede acceder al estado con el método `getState()`
- Se puede actualizar el estado con el método `dispatch(action)`
- Escucha cambios con el método - `subscribe(listener)`
- Deja de escuchar cambios retornando la función del método `subscribe(listener)`


El storage se importa de la siguiente forma:

```
import { createStorage } from 'redux'
```

Este método recibe dos parámetros:

- Reducer => Función pura que retorna el próximo estado.
- PreloadState / InitialState => Es el estado inicial de la aplicación, la primera carga, el llamado a una data. Puede ser cualquier tipo de dato.
- Enhancer => Función que puede extender redux con capacidades añadidas por librerías externas. Es opcional. Eg. 

Además, debemos crear una constante que será la que utilicemos en la aplicación.

```
const store = createStore(
  reducer,
  initialState,
  enhancer
)
```

PD: La arrow function de state puede quedar en state => state

### Clase 9
Las acciones se utilizan con el método `dispatch()`
Son la única fuente de información del store. Solamente de esa forma el store puede saber que los states cambian.
Son objetos planos
```
{
  store.dispatch({
    type: 'ADD_SONG',
    payload: 'Despacito'// Optional
})
```

### Clase 10
#### **Reducer**

- Modifican nuestro estado
- Puede haber múltiples reducers en la aplicación y un solo store.
- Devuelve el siguiente estado.

_Que no deben hacer_

- Modificar los argumentos recibidos
- Llamar a APIs (u otras tareas secundarias)
- Llamar a funciones no puras como `Date.now()`, `Math.random()`

#### Funciones puras

Es un concepto de programación funcional, hace que el código sea más legible. Tienen las siguientes condiciones:
Dados los mismos datos de entrada, deben retornar el mismo resultado sin importar el número de veces que se llame.
No debe tener objetos secundarios.
El reducer se iguala a una función, esta función recibe dos parámetros que son el state y la action y en la función se debe definir que se hace con state y action; generalmente se valida con un switch.

```
const reducer = function(state, action) {
    switch(action.type) {
        case 'ADD_SONG':
            return [...state, {title: action.payload}]
        default: 
            return state
    }
}
```

### Clase 11
Resume del flujo de eventos

- Se establece un State (estado) inicial con el que se crea originalmente el store mediante el método createStore de redux
- el state inicial define los contenidos que se muestran en la UI mediante store.getState() y la función render()
mediante los elementos de la UI se dispara un evento que se captura mediante un listener (como onsubmit) y que invoca al store.dispatch() con una acción específica …
- el Reducer recibe la acción(type y payload) enviada mediante el store.dispatch() y genera un nuevo estado que remplazará al estado inicial (o anterior)
- el cambio del estado es detectado por el store y se ejecuta store.subscribe(handle) … con la función (handle) que manejará lo que sucede cuando ha cambiado el estado
- en la función (handle) recibida por store.subscribe(handle) se invoca la actualización de la UI a partir del nuevo estado creado
- queda establecido el nuevo estado(state) y queda de nuevo atento el evento (listener) en la UI