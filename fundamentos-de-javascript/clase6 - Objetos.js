// var nombre = 'Emmanuel'


/* 
    function imprimirNombreEnMayusculas (nombre) {
        console.log(nombre)
        nombre = nombre.toUpperCase();
        console.log(nombre)
    }  
    imprimirNombreEnMayusculas('Manu');
    imprimirNombreEnMayusculas('Darío');
    imprimirNombreEnMayusculas('José');
 */

// Para no tener que hacer todas estas llamadas y definir mil variables, podemos hacer un objeto: 

var emmanuel = {
    nombre: 'Emmanuel',
    apellido: 'Martínez',
    edad: 32
}

var dario = {
    nombre: 'Darío',
    apellido: 'Salvat',
    ead: 25
}  

function imprimirNombreEnMayusculas(persona) {
    var nombre = persona.nombre.toUpperCase();
    console.log(nombre)
}

imprimirNombreEnMayusculas(emmanuel)
imprimirNombreEnMayusculas(dario)

// Esta forma de desglosar el objeto dentro del atributo de la función es una característica de 
// las nuevas versiones de JavaScript
function desglosandoObjetos({ nombre }) {
    var n = nombre.toUpperCase();
    console.log(n)
}
desglosandoObjetos(emmanuel)
desglosandoObjetos({nombre: 'Juan'})