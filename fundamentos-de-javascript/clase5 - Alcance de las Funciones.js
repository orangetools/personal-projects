var nombre = 'Emmanuel'

function imprimirNombreEnMayusculas (nombre) {
    console.log(nombre)
    nombre = nombre.toUpperCase();
    console.log(nombre)
}

imprimirNombreEnMayusculas(nombre);

// Esto que sigue no funciona, porque nombreEnMayusculas no esta disponible para fuera de la funcion.
// Solo vive dentro del Scope de la función.
// console.log(nombreEnMayusculas)